package com.example.used_back.service.impl;

import com.example.used_back.dao.UserMapper;
import com.example.used_back.pojo.User;
import com.example.used_back.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Resource
    private UserMapper userMapper;

    //根据用户账号查询用户信息
    @Override
    public User selectUserByAccount(String account) {
        return userMapper.selectUserByAccount(account);
    }

    //注册用户或添加其他角色
    @Override
    public boolean addUserOrOtherRole(User user) {
        userMapper.addUserOrOtherRole(user);
        return true;
    }

    @Override
    public User selectUserById(Integer id) {
        return userMapper.selectUserById(id);
    }

    //根据id修改用户信息
    @Override
    public boolean updateUserMessageById(User user) {
        userMapper.updateUserMessageById(user);
        return true;
    }

    //根据id修改用户余额或者角色
    @Override
    public int updateUserRoleOrBalanceById(String role, Double balance,Integer id) {
        try {
            userMapper.updateUserRoleOrBalanceById(role,balance,id);
            return 1;
        }catch (Exception e){
            System.out.println("修改失败，失败信息："+e);
            return 0;
        }
    }

    //根据账号和角色模糊分页查询所有用户信息
    @Override
    public List<User> selectUserByAccountAndeRole(String account, String role, Integer pageNum, Integer pageSize) {
        return userMapper.selectUserByAccountAndeRole(account,role,pageNum,pageSize);
    }

    //查询所有用户信息
    @Override
    public List<User> selectAllUser() {
        return userMapper.selectAllUser();
    }

    //统计角色
    @Override
    public List<User> selectRoleGroup() {
        return userMapper.selectRoleGroup();
    }

    //逻辑删除用户
    @Override
    public int deleteUserAllMessageById(Integer id) {
        return userMapper.deleteUserAllMessageById(id);
    }

    @Override
    public List<User> uploadUser() {
        return userMapper.uploadUser();
    }
}

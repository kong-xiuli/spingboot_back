/*
 Navicat Premium Data Transfer

 Source Server         : 毕设
 Source Server Type    : MySQL
 Source Server Version : 50730
 Source Host           : localhost:3308
 Source Schema         : test

 Target Server Type    : MySQL
 Target Server Version : 50730
 File Encoding         : 65001

 Date: 26/05/2022 19:40:47
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for back
-- ----------------------------
DROP TABLE IF EXISTS `back`;
CREATE TABLE `back`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `price` decimal(10, 2) NULL DEFAULT NULL COMMENT '价格',
  `isput` int(11) NOT NULL DEFAULT 1 COMMENT '是否上架1上架0下架',
  `isrent` int(11) NOT NULL DEFAULT 0 COMMENT '是否租赁0未租赁1已租赁',
  `createby` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `createtime` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '上架时间',
  `isdelete` int(255) NOT NULL DEFAULT 0 COMMENT '是否删除1删除0未删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of back
-- ----------------------------
INSERT INTO `back` VALUES (1, 1.33, 0, 0, 'll', '2022-02-16 21:55:44', 0);
INSERT INTO `back` VALUES (2, 1.33, 1, 0, 'll', '2022-03-08 20:19:12', 0);
INSERT INTO `back` VALUES (3, 1.33, 1, 0, 'll', '2022-03-08 20:19:27', 0);
INSERT INTO `back` VALUES (4, 2.00, 0, 0, 'll', '2022-03-08 20:20:20', 1);
INSERT INTO `back` VALUES (5, 2.00, 1, 0, 'll', '2022-03-08 20:20:30', 0);
INSERT INTO `back` VALUES (6, 1.33, 1, 0, 'll', '2022-03-08 20:20:40', 0);
INSERT INTO `back` VALUES (7, 1.33, 1, 0, 'll', '2022-03-08 20:20:48', 0);
INSERT INTO `back` VALUES (9, 9.00, 1, 1, 'kk', '2022-03-12 14:40:23', 0);
INSERT INTO `back` VALUES (10, 999.00, 0, 0, 'kk', '2022-03-12 14:56:37', 1);
INSERT INTO `back` VALUES (11, 2.00, 0, 0, 'll', '2022-03-23 19:54:00', 0);
INSERT INTO `back` VALUES (12, 2.00, 0, 0, 'll', '2022-03-23 19:57:42', 0);
INSERT INTO `back` VALUES (13, 6.00, 0, 0, 'll', '2022-04-09 14:34:02', 0);

-- ----------------------------
-- Table structure for back_order
-- ----------------------------
DROP TABLE IF EXISTS `back_order`;
CREATE TABLE `back_order`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `uid` int(11) NOT NULL COMMENT '用户id',
  `uname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `price` decimal(10, 2) NULL DEFAULT NULL COMMENT '价格',
  `createtime` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '下单时间',
  `ispay` int(11) NOT NULL DEFAULT 0 COMMENT '是否支付1支付0未支付',
  `isfinish` int(255) UNSIGNED NULL DEFAULT 0 COMMENT '是否完成订单1完成0未完成',
  `isdelete` int(255) NOT NULL DEFAULT 0 COMMENT '是否删除1删除0未删除',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `uid`(`uid`) USING BTREE,
  INDEX `uname`(`uname`) USING BTREE,
  CONSTRAINT `back_order_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `back_order_ibfk_2` FOREIGN KEY (`uname`) REFERENCES `user` (`uname`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of back_order
-- ----------------------------
INSERT INTO `back_order` VALUES (1, 3, 'lisa', 2.00, '2022-02-03 15:56:16', 0, 0, 0);
INSERT INTO `back_order` VALUES (2, 3, 'lisa', 2.00, '2020-10-16 15:59:19', 1, 1, 0);
INSERT INTO `back_order` VALUES (3, 4, 'oo', 2.00, '2021-03-18 16:01:47', 1, 1, 1);
INSERT INTO `back_order` VALUES (4, 4, 'oo', 1.33, '2022-03-08 20:23:28', 1, 1, 0);
INSERT INTO `back_order` VALUES (6, 4, 'oo', 7.20, '2022-03-12 16:09:04', 0, 0, 0);
INSERT INTO `back_order` VALUES (9, 3, 'lisa', 1.98, '2022-03-23 20:09:18', 0, 0, 0);
INSERT INTO `back_order` VALUES (10, 4, 'oo', 1.60, '2022-04-08 19:00:32', 1, 1, 0);
INSERT INTO `back_order` VALUES (11, 4, 'oo', 8.91, '2022-04-08 19:07:05', 1, 1, 0);
INSERT INTO `back_order` VALUES (12, 4, 'oo', 1.98, '2022-04-08 19:07:07', 1, 1, 0);
INSERT INTO `back_order` VALUES (13, 4, 'oo', 1.60, '2022-04-08 19:08:31', 0, 0, 0);
INSERT INTO `back_order` VALUES (14, 4, 'oo', 1.60, '2022-04-08 19:08:35', 0, 0, 0);
INSERT INTO `back_order` VALUES (15, 4, 'oo', 1.06, '2022-04-08 19:08:40', 0, 0, 0);
INSERT INTO `back_order` VALUES (16, 4, 'oo', 1.06, '2022-04-08 19:08:42', 0, 0, 0);
INSERT INTO `back_order` VALUES (17, 4, 'oo', 8.91, '2022-04-09 14:28:51', 1, 1, 1);

-- ----------------------------
-- Table structure for fault
-- ----------------------------
DROP TABLE IF EXISTS `fault`;
CREATE TABLE `fault`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `descs` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '描述',
  `uid` int(11) NULL DEFAULT NULL COMMENT '用户id',
  `uname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `did` int(11) NULL DEFAULT NULL COMMENT '处理人id',
  `dealby` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '处理人',
  `isdeal` int(11) NOT NULL DEFAULT 0 COMMENT '是否处理1未处理0已处理',
  `createtime` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `isdelete` int(255) NULL DEFAULT 0 COMMENT '是否删除1删除0未删除',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `uid`(`uid`) USING BTREE,
  INDEX `uname`(`uname`) USING BTREE,
  INDEX `did`(`did`) USING BTREE,
  INDEX `dealby`(`dealby`) USING BTREE,
  CONSTRAINT `fault_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fault_ibfk_2` FOREIGN KEY (`uname`) REFERENCES `user` (`uname`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fault_ibfk_3` FOREIGN KEY (`did`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fault_ibfk_4` FOREIGN KEY (`dealby`) REFERENCES `user` (`uname`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of fault
-- ----------------------------
INSERT INTO `fault` VALUES (1, '自行车有的太旧了', 3, 'lisa', 1, 'kk', 1, '2022-02-28 00:00:00', 0);
INSERT INTO `fault` VALUES (2, '自行车样式应该多一点', 4, 'oo', 2, 'll', 1, '2022-02-16 00:00:00', 1);
INSERT INTO `fault` VALUES (3, '333', 4, 'oo', 1, 'kk', 0, '2022-04-01 20:52:11', 0);
INSERT INTO `fault` VALUES (5, '自行车坏了', 4, 'oo', 1, 'kk', 1, '2022-03-06 10:08:04', 1);
INSERT INTO `fault` VALUES (6, '66666', 4, 'oo', 1, 'kk', 1, '2022-03-06 10:08:32', 1);
INSERT INTO `fault` VALUES (9, '我喜欢粉色一点的车子', 4, 'oo', NULL, NULL, 0, '2022-03-06 14:49:27', 0);
INSERT INTO `fault` VALUES (10, '希望地铁附近多设置点租赁点', 10, '九九', NULL, NULL, 0, '2022-03-08 20:13:35', 0);
INSERT INTO `fault` VALUES (11, '111111', 4, 'oo', 1, 'kk', 1, '2022-03-12 14:35:11', 1);
INSERT INTO `fault` VALUES (14, 'ceshi009', 4, 'oo', NULL, NULL, 0, '2022-04-09 14:30:28', 0);

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `rname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '角色名称',
  `descs` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '角色描述',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES (1, '超级管理员', '可以查看角色，处理故障信息，查看用户信息，将用户授权为管理员或超级管理员，对管理员授权为超级管理员，对用户进行删除或者修改操作');
INSERT INTO `role` VALUES (2, '管理员', '可以处理故障信息，对自行车进行删除、上架、下架、添加操作');
INSERT INTO `role` VALUES (4, '高级会员', '可以进行钱包充值，修改个人新信息，提出故障，享受角色对应的优惠价格');
INSERT INTO `role` VALUES (5, '超级会员', '可以进行钱包充值，修改个人新信息，提出故障，享受角色对应的优惠价格');
INSERT INTO `role` VALUES (6, '普通会员', '可以进行钱包充值，修改个人新信息，提出故障，享受角色对应的优惠价格');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `uname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '真实姓名',
  `account` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '账号',
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '密码',
  `sex` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '性别',
  `age` int(255) NULL DEFAULT NULL COMMENT '年龄',
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '电话',
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `role` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '角色',
  `balance` double(20, 2) UNSIGNED NULL DEFAULT 0.00 COMMENT '用户余额',
  `status` int(255) NULL DEFAULT 1 COMMENT '状态1存在0删除',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `uname`(`uname`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 'kk', 'kk', '123456', '女', 18, '18678787779', '24489@qq.com', '超级管理员', 0.00, 1);
INSERT INTO `user` VALUES (2, 'll', 'll', '666666', '男', 29, '19787876666', '55@qq.com', '管理员', 0.00, 1);
INSERT INTO `user` VALUES (3, 'lisa', 'lisa', '123666', '女', 16, '17676767777', '33@qq.com', '普通会员', 0.00, 1);
INSERT INTO `user` VALUES (4, 'oo', 'oo', '666666', '男', 23, '15333969697', 'oo@qq.com', '普通会员', 0.00, 1);
INSERT INTO `user` VALUES (6, 'wang', 'wang', '66666', '女', 16, '18888888888', '', '普通会员', 0.00, 1);
INSERT INTO `user` VALUES (7, 'uu', 'uu', '77777', '女', 99, '17888888888', '', '高级会员', 66.00, 1);
INSERT INTO `user` VALUES (8, 'ghjg', 'mjjg', '88888', '男', 88, '19888888888', '', '普通会员', 13.00, 0);
INSERT INTO `user` VALUES (9, 'zz', 'zz', 'zzzzz', '男', 88, '18888888888', NULL, '超级会员', 88.00, 1);
INSERT INTO `user` VALUES (10, '九九', '99', '99999', '男', 23, '19999999999', '', '普通会员', 0.00, 1);
INSERT INTO `user` VALUES (11, 'two', '22', '22222', '男', 22, '13222222222', NULL, '普通会员', 0.00, 0);
INSERT INTO `user` VALUES (12, 'sevena', '777a', '777778', '女', 33, '18777777778', NULL, '普通会员', 0.00, 1);
INSERT INTO `user` VALUES (14, 'yy', 'yy', 'yyyyy', '女', 23, '16855555555', '', '普通会员', 0.00, 1);
INSERT INTO `user` VALUES (15, '测试', '测试01', '12121', '女', 22, '18333333333', NULL, '普通会员', 0.00, 1);
INSERT INTO `user` VALUES (16, 'tt', 'tt', '333333', '男', 22, '13888888888', 'oo@qq.com', '超级管理员', 0.00, 1);

SET FOREIGN_KEY_CHECKS = 1;

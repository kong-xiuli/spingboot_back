package com.example.used_back.controller;

import com.example.used_back.pojo.Role;
import com.example.used_back.service.RoleService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
public class RoleController {

    @Resource
    private RoleService roleService;

    // 查询所有故障信息
    @PostMapping("/getAllRole")
    @ResponseBody
    public List<Role> getAllRoleMessage(){
        try {
            //查询所有角色信息
            List<Role> roles = roleService.selectAllRole();
            System.out.println("获取所有角色信息成功！"+roles);
            //返回结果
            return roles;
        } catch (Exception e){
            System.out.println("获取所有角色信息失败！" + e);
            return null;
        }
    }
}

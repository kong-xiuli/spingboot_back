package com.example.used_back.dao;

import com.example.used_back.pojo.Fault;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface FaultMapper {

    //新增故障信息
    int addFault(Fault fault);

    //根据id查询故障信息
    List<Fault> selectFaultById(Integer uid);

    //查询所有故障信息
    List<Fault> selectAllFromFault();

    //根据id删除故障信息
    int deleteFaultById(Integer id);

    //根据id修改故障状态及修改信息
    int updateFaultOfDealById(String dealby,Integer did,Integer id);
}
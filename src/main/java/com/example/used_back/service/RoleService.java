package com.example.used_back.service;

import com.example.used_back.pojo.Role;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface RoleService {

    //查询所有角色
    List<Role> selectAllRole();

}

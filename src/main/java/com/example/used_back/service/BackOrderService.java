package com.example.used_back.service;

import com.example.used_back.pojo.BackOrder;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
public interface BackOrderService {

    //根据用户名分页查询所有车辆订单（根据根据创建时间倒序）
    List<BackOrder> selectAllBackOrderByUname(String uname,Integer pageNum, Integer pageSize);

    //根据id分页查询对应订单(创建时间倒序)
    List<BackOrder> selectBackOrderById(Integer uid,Integer pageNum,Integer pageSize);

    //根据id删除订单信息
    int deleteBackOrderAllMessageById(Integer id);

    //查询所有订单
    List<BackOrder> selectAllBackOrder();

    //根据id查询所有订单信息
    List<BackOrder> selectAllBackOrderById(Integer uid);

    //租赁自行车 修改车辆信息，添加订单
    int editBackMessageAndAddBackOrder(Integer isrent, Integer id, Integer uid, String uname, BigDecimal price);

    //添加订单信息
    int addNewBackOrder(BackOrder backOrder);

    //根据id修改订单状态
    int updateBackOrderFinished(Integer id);

    //完成订单支付
    int finishOrder(String role,Double balance,Integer id,Integer bid);

    //查询订单数据
    List<BackOrder> selectOrderGroup(Integer uid);

    //导出订单数据
    List<BackOrder> uploadOrders();
}

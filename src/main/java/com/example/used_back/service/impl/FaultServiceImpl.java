package com.example.used_back.service.impl;

import com.example.used_back.dao.FaultMapper;
import com.example.used_back.pojo.Fault;
import com.example.used_back.service.FaultService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class FaultServiceImpl implements FaultService {

    @Resource
    private FaultMapper faultMapper;

    //根据id查询故障信息
    @Override
    public List<Fault> selectFaultById(Integer uid) {
        return faultMapper.selectFaultById(uid);
    }

    //查询所有故障信息
    @Override
    public List<Fault> selectAllFromFault() {
        return faultMapper.selectAllFromFault();
    }

    //根据id逻辑删除故障信息
    @Override
    public int deleteFaultById(Integer id) {
        return faultMapper.deleteFaultById(id);
    }

    //根据id修改故障信息状态及处理人
    @Override
    public int updateFaultOfDealById(String dealby, Integer did, Integer id) {
        try{
            faultMapper.updateFaultOfDealById(dealby,did,id);
            return 0;
        }catch (Exception e){
            System.out.println("错误信息----"+e);
            return 1;
        }
    }

    //添加故障信息
    @Override
    public int addFault(Fault fault) {
        return faultMapper.addFault(fault);
    }
}

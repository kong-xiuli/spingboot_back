package com.example.used_back.service.impl;

import com.example.used_back.dao.BackMapper;
import com.example.used_back.dao.BackOrderMapper;
import com.example.used_back.dao.UserMapper;
import com.example.used_back.pojo.BackOrder;
import com.example.used_back.service.BackOrderService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;

@Service
public class BackOrderServiceImpl implements BackOrderService {

    @Resource
    private BackOrderMapper backOrderMapper;
    @Resource
    private BackMapper backMapper;
    @Resource
    private UserMapper userMapper;

    //根据用户名分页查询所有车辆订单（根据根据创建时间倒序）
    @Override
    public List<BackOrder> selectAllBackOrderByUname(String uname,Integer pageNum, Integer pageSize) {
        try {
            return backOrderMapper.selectAllBackOrderByUname(uname,pageNum,pageSize);
        }catch (Exception e){
            return null;
        }

    }

    //根据id分页查询对应订单(创建时间倒序)
    @Override
    public List<BackOrder> selectBackOrderById(Integer uid,Integer pageNum,Integer pageSize) {
        return backOrderMapper.selectBackOrderById(uid,pageNum,pageSize);
    }

    //根据id删除订单信息
    @Override
    public int deleteBackOrderAllMessageById(Integer id) {
        return backOrderMapper.deleteBackOrderAllMessageById(id);
    }

    //查询所有订单
    @Override
    public List<BackOrder> selectAllBackOrder() {
        return backOrderMapper.selectAllBackOrder();
    }

    //根据id查询所有订单信息
    @Override
    public List<BackOrder> selectAllBackOrderById(Integer uid) {
        return backOrderMapper.selectAllBackOrderById(uid);
    }


    //租赁自行车 修改车辆信息、添加订单
    @Override
    @Transactional(propagation= Propagation.REQUIRED,readOnly=false,isolation= Isolation.READ_COMMITTED,rollbackForClassName="Exception")
    public int editBackMessageAndAddBackOrder(Integer isrent, Integer id, Integer uid, String uname, BigDecimal price) {
            BackOrder backOrders = new BackOrder();
            backOrders.setUid(uid);
            backOrders.setUname(uname);
            backOrders.setPrice(price);
            //生成对应订单
            backOrderMapper.addNewBackOrder(backOrders);
            //修改自行车状态
            return backMapper.updateBackRentMessageById(isrent,id);
    }


    //完成订单支付
    @Override
    @Transactional(propagation= Propagation.REQUIRED,readOnly=false,isolation= Isolation.READ_COMMITTED,rollbackForClassName="Exception")
    public int finishOrder(String role, Double balance, Integer id, Integer bid) {
        System.out.println(role);
        System.out.println(balance);
        System.out.println(id);
        System.out.println(bid);
            //修改自行车状态
            backOrderMapper.updateBackOrderFinished(bid);
            //修改用户余额
            return userMapper.updateUserRoleOrBalanceById(role,balance,id);

    }

    //查询订单数据
    @Override
    public List<BackOrder> selectOrderGroup(Integer uid) {
        return backOrderMapper.selectOrderGroup(uid);
    }

    //获取导出订单数据
    @Override
    public List<BackOrder> uploadOrders() {
        return backOrderMapper.uploadOrders();
    }

    //添加订单
    @Override
    public int addNewBackOrder(BackOrder backOrder) {
        return backOrderMapper.addNewBackOrder(backOrder);
    }

    //根据id修改订单状态
    @Override
    public int updateBackOrderFinished(Integer id) {
        return backOrderMapper.updateBackOrderFinished(id);
    }
}

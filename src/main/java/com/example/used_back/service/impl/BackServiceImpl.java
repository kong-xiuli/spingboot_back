package com.example.used_back.service.impl;

import com.example.used_back.dao.BackMapper;
import com.example.used_back.pojo.Back;
import com.example.used_back.service.BackService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class BackServiceImpl implements BackService {

    @Resource
    private BackMapper backMapper;

    //根据价格模糊查询所有车辆信息(根据价格倒序排序)
    @Override
    public List<Back> selectBackByPriceDesc(Integer pageNum,Integer pageSize) {
        return backMapper.selectBackByPriceDesc(pageNum,pageSize);
    }

    //查询所有车辆信息
    @Override
    public List<Back> selectAllBack() {
        return backMapper.selectAllBack();
    }

    //根据id租赁车辆,修改租赁状态
    @Override
    public int updateBackRentMessageById(Integer isrent, Integer id) {
        return backMapper.updateBackRentMessageById(isrent,id);
    }

    //根据id修改车辆上架下架状态
    @Override
    public int updateBackPutMessageById(Integer isput, Integer id) {
        return backMapper.updateBackPutMessageById(isput,id);
    }

    //根据id逻辑删除车辆信息
    @Override
    public int deleteBackAllMessageById(Integer id) {
        return backMapper.deleteBackAllMessageById(id);
    }

    //分页查询所有车辆信息 按照价格降序排序 可租赁状态的
    @Override
    public List<Back> selectRentBackByPriceDesc(Integer pageNum,Integer pageSize) {
        return backMapper.selectRentBackByPriceDesc(pageNum,pageSize);
    }

    //查询所有可用车辆的信息
    @Override
    public List<Back> selectAllUsedBack() {
        return backMapper.selectAllUsedBack();
    }


    //添加车辆
    @Override
    public int addNewBack(Back back) {
        return backMapper.addNewBack(back);
    }

    //根据id修改车俩信息
    @Override
    public int updateBackById(Back back) {
        try {
            backMapper.updateBackById(back);
            return 0;
        }catch (Exception e){
            System.out.println("此u该自行车错误信息:"+e);
            return 1;
        }
    }
}

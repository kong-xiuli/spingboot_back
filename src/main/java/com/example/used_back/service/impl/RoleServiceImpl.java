package com.example.used_back.service.impl;

import com.example.used_back.dao.RoleMapper;
import com.example.used_back.pojo.Role;
import com.example.used_back.service.RoleService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {
    @Resource
    private RoleMapper roleMapper;

    //查询所有角色
    @Override
    public List<Role> selectAllRole() {
        return roleMapper.selectAllRole();
    }
}

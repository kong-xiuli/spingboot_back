package com.example.used_back.controller;

import com.example.used_back.pojo.Fault;
import com.example.used_back.service.FaultService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
public class FaultController {
    @Resource
    private FaultService faultService;

    // 查询所有故障信息
    @PostMapping("/getAllFault")
    @ResponseBody
    public List<Fault> getAllFaultMessage(){
        try {
            List<Fault> fault = faultService.selectAllFromFault();
            System.out.println("获取所有故障信息成功！"+fault);
            return fault;
        } catch (Exception e){
            System.out.println("获取所有故障信息失败！" + e);
            return null;
        }
    }

    // 根据id查询故障信息
    @RequestMapping("/getFault")
    @ResponseBody
    public List<Fault> getFaultMessageById(Integer uid){
        System.out.println("id:"+uid);
        try {
            List<Fault> fault = faultService.selectFaultById(uid);
            System.out.println("fault"+fault);
            return fault;
        } catch (Exception e){
            System.out.println("获取故障信息失败！" + e);
            return null;
        }
    }

    // 根据id删除故障信息
    @RequestMapping("/deleteFault")
    @ResponseBody
    public String deleteFaultById(Integer id){
        System.out.println("id:"+id);
        try {
            faultService.deleteFaultById(id);
             return "删除成功！";
        } catch (Exception e){
            return "删除失败!";
        }
    }

    //根据id修改故障状态及修改信息
    @RequestMapping("/updateFault")
    @ResponseBody
    public String updateFaultOfDealById(Fault fault){
        System.out.println("接收到的信息:"+fault);
        try {
            faultService.updateFaultOfDealById(fault.getDealby(),fault.getDid(),fault.getId());
            return "阅读成功！";
        } catch (Exception e){
            return "阅读失败!";
        }
    }

    //添加故障信息
    // 查询所有故障信息
    @PostMapping("/addFault")
    @ResponseBody
    public String addFault(@RequestBody Fault fault){
        try {
           faultService.addFault(fault);
           return "添加故障信息成功！";
        } catch (Exception e){
            System.out.println("添加故障信息失败！" + e);
            return "添加故障信息失败！";
        }
    }
}

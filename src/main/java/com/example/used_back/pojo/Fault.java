package com.example.used_back.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import java.util.Date;


@Setter
@Getter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Fault {


    private Integer id;

    private String descs;

    private Integer uid;

    private String uname;

    private Integer did;

    private String dealby;

    private Integer isdeal;

    private Integer isdelete;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createtime;

    //public Fault(Integer id, String descs, Integer uid, String uname, Integer did, String dealby, Integer isdeal, Integer isdelete, Date createtime) {
    //    this.id = id;
    //    this.descs = descs;
    //    this.uid = uid;
    //    this.uname = uname;
    //    this.did = did;
    //    this.dealby = dealby;
    //    this.isdeal = isdeal;
    //    this.isdelete = isdelete;
    //    this.createtime = createtime;
    //}
    //
    //@Override
    //public String toString() {
    //    return "Fault{" +
    //            "id=" + id +
    //            ", descs='" + descs + '\'' +
    //            ", uid=" + uid +
    //            ", uname='" + uname + '\'' +
    //            ", did=" + did +
    //            ", dealby='" + dealby + '\'' +
    //            ", isdeal=" + isdeal +
    //            ", isdelete=" + isdelete +
    //            ", createtime=" + createtime +
    //            '}';
    //}
    //
    //public Integer getId() {
    //    return id;
    //}
    //
    //public void setId(Integer id) {
    //    this.id = id;
    //}
    //
    //public String getDescs() {
    //    return descs;
    //}
    //
    //public void setDescs(String descs) {
    //    this.descs = descs;
    //}
    //
    //public Integer getUid() {
    //    return uid;
    //}
    //
    //public void setUid(Integer uid) {
    //    this.uid = uid;
    //}
    //
    //public String getUname() {
    //    return uname;
    //}
    //
    //public void setUname(String uname) {
    //    this.uname = uname;
    //}
    //
    //public Integer getDid() {
    //    return did;
    //}
    //
    //public void setDid(Integer did) {
    //    this.did = did;
    //}
    //
    //public String getDealby() {
    //    return dealby;
    //}
    //
    //public void setDealby(String dealby) {
    //    this.dealby = dealby;
    //}
    //
    //public Integer getIsdeal() {
    //    return isdeal;
    //}
    //
    //public void setIsdeal(Integer isdeal) {
    //    this.isdeal = isdeal;
    //}
    //
    //public Integer getIsdelete() {
    //    return isdelete;
    //}
    //
    //public void setIsdelete(Integer isdelete) {
    //    this.isdelete = isdelete;
    //}
    //
    //public Date getCreatetime() {
    //    return createtime;
    //}
    //
    //public void setCreatetime(Date createtime) {
    //    this.createtime = createtime;
    //}
}
package com.example.used_back.controller;

import com.example.used_back.pojo.Back;
import com.example.used_back.service.BackOrderService;
import com.example.used_back.service.BackService;
import jdk.nashorn.internal.runtime.Debug;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;

@Controller
public class BackController {

    @Resource
    private BackService backService;
    @Resource
    private BackOrderService backOrderService;

    //分页模糊查询车辆信息
    @RequestMapping("getBack")
    @ResponseBody
    public List<Back> selectBackByPriceDesc(Integer pageNum,Integer pageSize){
        try {
            List<Back> backs = backService.selectBackByPriceDesc((pageNum-1)*pageSize,pageSize);
            return backs;
        }catch (Exception e){
            System.out.println("分页查询所有测含量信息的错误信息:"+ e);
            return null;
        }
    }

    //查询所有车辆信息
    @RequestMapping("getAllBack")
    @ResponseBody
    public List<Back> getAllBack(){
        try {
            List<Back> backs = backService.selectAllBack();
            return backs;
        }catch (Exception e){
            System.out.println("获取所有车辆信息的错误信息:"+ e);
            return null;
        }
    }


    //根据id租赁车辆,修改租赁状态于此同时生成一个对应的订单
    @RequestMapping("updateRent")
    @ResponseBody
    public String updateBackRentMessageById(Integer isrent, Integer id, Integer uid, String uname, BigDecimal price){
        try {
            backOrderService.editBackMessageAndAddBackOrder(isrent,id,uid,uname,price);
            return "租车成功！";
        }catch (Exception e){
            System.out.println("租车错误信息"+e);
            return "租车失败！";
        }
    }

    //根据id修改车辆上架下架状态
    @RequestMapping("updatePut")
    @ResponseBody
    public String updateBackPutMessageById(Integer isput,Integer id){
        try {
            backService.updateBackPutMessageById(isput,id);
            return "修改上架下架状态成功！";
        }catch (Exception e){
            System.out.println("错误信息:"+ e);
            return "修改上架下架状态失败！";
        }
    }

    //根据id逻辑删除车辆信息
    @RequestMapping("deleteBack")
    @ResponseBody
    public String deleteBackAllMessageById(Integer id){
        try {
            backService.deleteBackAllMessageById(id);
            return "删除成功！";
        }catch (Exception e){
            System.out.println("错误信息:"+ e);
            return "删除失败！";
        }
    }

    //分页模糊查询车辆信息
    @RequestMapping("getUsedBack")
    @ResponseBody
    public List<Back> selectRentBackByPriceDesc(Integer pageNum,Integer pageSize){
        try {
            List<Back> backs = backService.selectRentBackByPriceDesc((pageNum-1)*pageSize,pageSize);
            return backs;
        }catch (Exception e){
            System.out.println("分页查询所有测含量信息的错误信息:"+ e);
            return null;
        }
    }

    //查询所有车辆信息
    @RequestMapping("getAllUsedBack")
    @ResponseBody
    public List<Back> selectAllUsedBack(){
        try {
            List<Back> backs = backService.selectAllUsedBack();
            return backs;
        }catch (Exception e){
            System.out.println("错误信息:"+ e);
            return null;
        }
    }

    //添加车辆
    @PostMapping("addBack")
    @ResponseBody
    public String addNewBack(@RequestBody Back back){
        try {
            backService.addNewBack(back);
            return "添加成功！";
        }catch (Exception e){
            return "添加失败！";
        }
    }

    //根据id修改车辆信息
    @PostMapping("editBack")
    @ResponseBody
    public String editBack(@RequestBody Back back){
       int res = backService.updateBackById(back);
        if (res == 0) {
            return "修改成功！";
        }else {
            return "修改失败！";
        }
    }

}

package com.example.used_back.service;

import com.example.used_back.pojo.User;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UserService {
    //根据用户账号查询用户信息
    User selectUserByAccount(String account);

    //注册用户或添加其他角色
    boolean addUserOrOtherRole(User user);

    //根据id获取用户详情信息
    User selectUserById(Integer id);

    //根据id修改用户信息
    boolean updateUserMessageById(User user);

    //根据id修改用户角色或者余额
    int updateUserRoleOrBalanceById(String role,Double balance,Integer id);

    //根据账号或角色分页查询信息
    List<User> selectUserByAccountAndeRole(String account, String role, Integer pageNum, Integer pageSize);

    //查询所有用户信息
    List<User> selectAllUser();

    //统计角色
    List<User> selectRoleGroup();

    //根据id逻辑删除用户
    int deleteUserAllMessageById(Integer id);

    //导出用户数据查询
    List<User> uploadUser();

}

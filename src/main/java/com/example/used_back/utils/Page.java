package com.example.used_back.utils;

import com.alibaba.excel.annotation.ExcelIgnore;

public class Page {
    @ExcelIgnore
    private Integer pageNum;
    @ExcelIgnore
    private Integer pageSize;

    public Integer getPageNum() {
        return pageNum;
    }

    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

}


package com.example.used_back.utils;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.write.metadata.WriteSheet;
import com.example.used_back.pojo.BackOrder;
import com.example.used_back.pojo.User;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class DownloadExcel {
    /**
     * 导出excel的方法
     * response 返回对象
     * list 导出数据
     */
    public static void writeOrdersExcel(HttpServletResponse response, List<BackOrder> list) throws IOException {
        //输出流
        ExcelWriter excelWriter = EasyExcel.write(response.getOutputStream()).build();
        //定义工作表对象
        WriteSheet writeSheet = EasyExcel.writerSheet(0,"sheet").head(BackOrder.class).build();
        //将数据写进excel表
        excelWriter.write(list,writeSheet);
        excelWriter.finish();
    }

    public static void writeUserExcel(HttpServletResponse response, List<User> list) throws IOException {
        //输出流
        ExcelWriter excelWriter = EasyExcel.write(response.getOutputStream()).build();
        //定义工作表对象
        WriteSheet writeSheet = EasyExcel.writerSheet(0,"sheet").head(User.class).build();
        //将数据写进excel表
        excelWriter.write(list,writeSheet);
        excelWriter.finish();
    }
}

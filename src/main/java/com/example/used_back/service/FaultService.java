package com.example.used_back.service;

import com.example.used_back.pojo.Fault;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface FaultService {
    // 根据id查询故障信息
    List<Fault> selectFaultById(Integer uid);

    // 查询所有故障信息
    List<Fault> selectAllFromFault();

    // 根据id删除故障信息
    int deleteFaultById(Integer id);

    //根据id修改故障状态及修改信息
    int updateFaultOfDealById(String dealby,Integer did,Integer id);

    //新增故障信息
    int addFault(Fault fault);

}

package com.example.used_back.dao;

import com.example.used_back.pojo.Role;
import com.example.used_back.pojo.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UserMapper {

    //根据id获取用户详情信息
    User selectUserById(Integer id);

    //根据id修改用户信息
    int updateUserMessageById(User user);

    //根据id修改用户角色或者余额
    int updateUserRoleOrBalanceById(String role,Double balance,Integer id);

    //查询所有用户信息
    List<User> selectAllUser();

    //根据用户名获取用户信息
    User selectUserByAccount(String account);

    //注册用户或添加其他角色
    int addUserOrOtherRole(User user);

    //根据账号或角色分页查询信息
    List<User> selectUserByAccountAndeRole(String account,String role,Integer pageNum,Integer pageSize);

    //根据id逻辑删除用户
    int deleteUserAllMessageById(Integer id);

    //统计角色
    List<User> selectRoleGroup();

    //导出用户数据查询
    List<User> uploadUser();

}
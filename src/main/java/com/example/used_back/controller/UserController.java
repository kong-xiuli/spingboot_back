package com.example.used_back.controller;

import com.example.used_back.pojo.BackOrder;
import com.example.used_back.pojo.User;
import com.example.used_back.service.UserService;
import com.example.used_back.utils.DownloadExcel;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@RestController
public class UserController {

    @Resource
    private UserService userService;

    // 用户登录判断
    @PostMapping(value = "/login")
    @ResponseBody
    public User login(@RequestBody User user1) {
        System.out.println("---------login判断：用户名：" + user1);
        // 根据用户账号查询用户信息
        User user = userService.selectUserByAccount(user1.getAccount());
        System.out.println("数据库匹配的用户：" + user);
        if (user != null) {
            String password1 = user.getPassword();
            // 密码对比
            if (password1.equals(user1.getPassword())) {
                System.out.println("登录成功!");
                return user;
            } else {
                System.out.println("密码错误!");
                return null;
            }
        } else {
            System.out.println("用户名不存在!");
            return null;
        }
    }


    // 用户注册或添加
    @PostMapping(value = "/register")
    @ResponseBody
    public String addUserOrOtherRole(@RequestBody User user) {
        try {
            System.out.println("注册用户信息"+user);
            //根据用户账号查询用户信息
            User user1 = userService.selectUserByAccount(user.getAccount());
            if(user1 != null){
                String uname = user1.getUname();
                //真实姓名也相等时
                if (uname.equals(user.getUname())){
                    return "用户已存在!";
                }
                return "账号名已存在!";
            }else {
                // 添加用户或者其他角色
                userService.addUserOrOtherRole(user);
                return "添加成功!";
            }
        }catch (Exception e) {
             e.printStackTrace();
             return "系统错误！添加失败！";
        }
    }

    // 根据id获取用户详情
    @RequestMapping("/getUserDetail")
    @ResponseBody
    public User getUserDetailById(Integer id){
        System.out.println("获取到的id:"+id+"-----查询结果:"+userService.selectUserById(id));
        return userService.selectUserById(id);
    }

    // 根据id修改用户信息
    @PostMapping("/updateUser")
    @ResponseBody
    public String updateUserMessageById(@RequestBody User user){
        System.out.println("获取到的User:"+user);
        try {
            userService.updateUserMessageById(user);
            return "修改信息成功！";
        } catch (Exception e){
            return "修改发生错误！";
        }
    }

    // 根据id修改用余额或者角色
    @PostMapping("/updateBalanceOrRole")
    @ResponseBody
    public String updateBalanceOrRoleById(@RequestBody User user){
        System.out.println("获取到的信息:"+user);
        userService.updateUserRoleOrBalanceById(user.getRole(),user.getBalance(),user.getId());
        return "已修改";
    }

    // 根据账号和角色分页查询信息
    @PostMapping("/getAllUser")
    @ResponseBody
    public List<User> selectUserByAccountAndeRole(@RequestBody User user){
        System.out.println("分页获取到的信息:"+user);
        try {
            List<User> users = userService.selectUserByAccountAndeRole(user.getAccount(),user.getRole(),(user.getPageNum()-1)*user.getPageSize(),user.getPageSize());
            System.out.println("查询结果："+users);
            return users;
        }catch (Exception e){
            System.out.println("错误信息:"+ e);
            return null;
        }
    }

    // 根据账号和角色分页查询信息
    @PostMapping("/selectAllUser")
    @ResponseBody
    public List<User> selectAllUser(){
        try {
            List<User> users = userService.selectAllUser();
            return users;
        }catch (Exception e){
            System.out.println("错误信息:"+ e);
            return null;
        }
    }

    // 根据id删除用户
    @RequestMapping("/deleteUser")
    @ResponseBody
    public String deleteUserAllMessageById(Integer id){
        System.out.println("id:"+id);
        try {
            userService.deleteUserAllMessageById(id);
            return "删除成功！";
        } catch (Exception e){
            return "删除失败!";
        }
    }

    //查询用户角色信息
    @RequestMapping("roleGroup")
    @ResponseBody
    public  List<?> selectRoleGroup(){
        try {
            List<?> list = userService.selectRoleGroup();
            System.out.println("查询结果："+list);
            return list;
        }catch (Exception e){
            System.out.println(e);
            return null;
        }
    }
    //用户数据导出
    @RequestMapping("uploadUsers")
    @ResponseBody
    public void uploadAllBackOrder(HttpServletResponse response) throws IOException {
        List<User> users = userService.uploadUser();
        //设置相应数据的输出流
        response.setContentType("application/vnd.ms-excel");
        //设置下载文件名称
        response.setHeader("Content-Disposition","attachment;filename="+"users.xlsx");
        //调用导出方法
        DownloadExcel.writeUserExcel(response,users);
    }
}

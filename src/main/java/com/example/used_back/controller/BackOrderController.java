package com.example.used_back.controller;

import com.example.used_back.pojo.BackOrder;
import com.example.used_back.service.BackOrderService;
import com.example.used_back.utils.DownloadExcel;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Controller
public class BackOrderController {

    @Resource
    private BackOrderService backOrderService;

    //分页查询所有订单信息(按照时间倒序)
    @RequestMapping("getAllBackOrder")
    @ResponseBody
    public List<BackOrder> selectAllBackOrderByUname(String uname, Integer pageNum, Integer pageSize) {
        try {
            System.out.println("页数" + (pageNum - 1) * pageSize);
            List<BackOrder> backOrders = backOrderService.selectAllBackOrderByUname(uname, (pageNum - 1) * pageSize, pageSize);
            System.out.println(backOrders);
            return backOrders;
        } catch (Exception e) {
            System.out.println("分页查询的错误信息:" + e);
            return null;
        }
    }

    //根据id分页查询所有订单信息(创建时间倒序)
    @RequestMapping("getBackOrder")
    @ResponseBody
    public List<BackOrder> selectAllBackOrder(Integer uid, Integer pageNum, Integer pageSize) {
        try {
            List<BackOrder> backOrders = backOrderService.selectBackOrderById(uid, (pageNum - 1) * pageSize, pageSize);
            return backOrders;
        } catch (Exception e) {
            System.out.println("错误信息:" + e);
            return null;
        }
    }

    //查询所有订单信息(管理员)
    @RequestMapping("getOrder")
    @ResponseBody
    public List<BackOrder> selectAllBackOrder() {
        try {
            List<BackOrder> backOrders = backOrderService.selectAllBackOrder();
            return backOrders;
        } catch (Exception e) {
            System.out.println("错误信息:" + e);
            return null;
        }
    }

    //根据id删除对应订单信息
    @RequestMapping("deleteBackOrder")
    @ResponseBody
    public String deleteBackOrderAllMessageById(Integer id) {
        try {
            if (id != null) {
                backOrderService.deleteBackOrderAllMessageById(id);
                return "删除成功！";
            } else {
                return "id不能为空！";
            }
        } catch (Exception e) {
            System.out.println("错误信息:" + e);
            return "删除失败！";
        }
    }


    //根据id查询所有订单信息(用户)
    @RequestMapping("getAllBackOrderByUid")
    @ResponseBody
    public List<BackOrder> selectAllBackOrderById(Integer uid) {
        try {
            List<BackOrder> backOrders = backOrderService.selectAllBackOrderById(uid);
            return backOrders;
        } catch (Exception e) {
            System.out.println("错误信息:" + e);
            return null;
        }
    }

    //完成订单支付
    @RequestMapping("pay")
    @ResponseBody
    public String finishOrder(String role, Double balance, Integer id, Integer bid) {
        try {
            backOrderService.finishOrder(role, balance, id, bid);
            return "支付成功！";
        } catch (Exception e) {
            System.out.println("错误信息:" + e);
            return "支付失败！";
        }
    }

    //查询订单价格统计信息
    @RequestMapping("orderGroup")
    @ResponseBody
    public List<?> selectOrderGroup(Integer uid) {
        try {
            List<?> list = backOrderService.selectOrderGroup(uid);
            System.out.println("查询结果：" + list);
            return list;
        } catch (Exception e) {
            System.out.println(e);
            return null;
        }
    }

    //订单数据导出
    @RequestMapping("uploadOrders")
    @ResponseBody
    public void uploadAllBackOrder(HttpServletResponse response) throws IOException {
            List<BackOrder> backOrders = backOrderService.uploadOrders();
            System.out.println(backOrders);
            //设置相应数据的输出流
            response.setContentType("application/vnd.ms-excel");
            //设置下载文件名称
            response.setHeader("Content-Disposition","attachment;filename="+"orders.xlsx");
            //调用导出方法
            DownloadExcel.writeOrdersExcel(response,backOrders);
    }
}

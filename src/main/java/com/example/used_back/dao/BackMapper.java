package com.example.used_back.dao;

import com.example.used_back.pojo.Back;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface BackMapper {

    //根据id修改车辆信息
    int updateBackById(Back back);

    //添加车辆
    int addNewBack(Back back);

    //分页查询所有车辆信息 按照价格降序排序 可租赁状态的
    List<Back> selectRentBackByPriceDesc(Integer pageNum,Integer pageSize);

    //根据id租赁车辆,修改租赁状态
    int updateBackRentMessageById(Integer isrent,Integer id);

    //根据id修改车辆上架下架状态
    int updateBackPutMessageById(Integer isput,Integer id);

    //根据id逻辑删除车辆信息
    int deleteBackAllMessageById(Integer id);

    //分页模糊查询车辆信息，按照价格降序
    List<Back> selectBackByPriceDesc(Integer pageNum,Integer pageSize);

   //查询所有车辆信息
    List<Back> selectAllBack();

    //查询所有可用车辆信息
    List<Back> selectAllUsedBack();
}
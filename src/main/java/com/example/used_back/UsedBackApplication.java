package com.example.used_back;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UsedBackApplication {

    public static void main(String[] args) {
        SpringApplication.run(UsedBackApplication.class, args);
        System.out.println("项目启动成功............");
    }

}
